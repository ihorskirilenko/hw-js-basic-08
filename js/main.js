'use strict'

/**
 * Опишіть своїми словами що таке Document Object Model (DOM)
 * Це модель, що реалізується як представлення стркутури веб-сторінки у вигляді ієрархічної сукупності об'єктів.
 * Відповідно, кожен елемент, що міститься на HTML-сторінці є об'єктом-нащадком глобального об'єкту document.
 *
 * Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 * innerHTML містить всю html-структуру елемента (включаючи нашадків): теги, властивості, стилі, контент...
 * innerText містить лише контент елемента та нащадків.
 *
 * Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
 * Для звернення до html-об'єкта існують вбудовані методи document:
 * getElementById, get ElementsByClass, getElementsByTagName, querySelector, querySelectorAll.
 * з точки зору універсальності та зручності краще використовувати querySelector, querySelectorAll.

 * Завдання
 * Код для завдань лежить в папці project.
 *
 * 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
 * 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
 * 3. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
 * 4. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
 * 5. Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль.
 * 6. Кожному з елементів присвоїти новий клас nav-item.
 * 7. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*
 */

//1
for (let item of document.querySelectorAll('p')) {
    item.style.backgroundColor = '#ff0000';
}

//2
console.log(document.querySelector('#optionsList'));
console.log(document.querySelector('#optionsList').parentElement);

//3
// One way...
console.log(document.querySelector('#optionsList').childNodes);

//Another way
if (document.querySelector('#optionsList').hasChildNodes()) {
    for (let item of document.querySelector('#optionsList').childNodes) {
        console.log(item);
        console.log(item.nodeType);
    }
}

//4
document.getElementById('testParagraph').innerText = 'This is paragraph';

//5, 6
for (let item of document.querySelector('.main-header').children) {
    console.log(item);
    item.classList.add('nav-item');
}

//7
for (let item of document.querySelectorAll('.section-title')) {
    item.classList.remove('section-title');
}


